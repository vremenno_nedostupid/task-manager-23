package ru.fedun.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CREATE TASK]");
        System.out.println("[ENTER TITLE:]");
        @NotNull final String title = TerminalUtil.nextLine();
        System.out.println("[ENTER DESCRIPTION:]");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final Session session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getTaskEndpoint().createTask(session, title, description);
        System.out.println("[OK]");
        System.out.println();
    }

}
