package ru.fedun.tm.command.data.binary;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;

public final class DataBinarySaveCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-save-bin";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data in binary file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BINARY SAVE]");
        @NotNull final Session session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getDataEndpoint().saveBinary(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
