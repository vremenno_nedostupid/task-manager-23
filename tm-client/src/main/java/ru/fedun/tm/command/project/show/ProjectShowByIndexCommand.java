package ru.fedun.tm.command.project.show;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.Project;
import ru.fedun.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-view_by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by index.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW PROJECT]");
        @NotNull final Session session = serviceLocator.getSessionService().getCurrentSession();
        System.out.println("[ENTER INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextInt() - 1;
        @NotNull final Project project = serviceLocator.getProjectEndpoint().showProjectByIndex(session, index);
        System.out.println("ID: " + project.getId());
        System.out.println("TITLE: " + project.getTitle());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
        System.out.println();
    }

}
