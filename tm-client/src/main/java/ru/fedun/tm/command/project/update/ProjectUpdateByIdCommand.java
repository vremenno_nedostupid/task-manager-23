package ru.fedun.tm.command.project.update;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-update-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by id.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PROJECT]");
        @NotNull final Session session = serviceLocator.getSessionService().getCurrentSession();
        System.out.println("[ENTER ID:]");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER TITLE:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectEndpoint().updateProjectById(session, id, name, description);
        System.out.println("[OK]");
        System.out.println();
    }

}
