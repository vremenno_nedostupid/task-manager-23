package ru.fedun.tm.command.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;

public final class DataJsonJaxbLoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-load-json-jb";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from json (jax-b) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON (JAX-B) LOAD]");
        @NotNull final Session session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getDataEndpoint().loadJsonByJaxb(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
