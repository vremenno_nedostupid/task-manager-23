package ru.fedun.tm.command.task.show;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.endpoint.Task;
import ru.fedun.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-view_by-index";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task by index.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[SHOW TASK]");
        @NotNull final Session session = serviceLocator.getSessionService().getCurrentSession();
        System.out.println("[ENTER INDEX:]");
        @NotNull final Integer index = TerminalUtil.nextInt() - 1;
        @NotNull final Task task = serviceLocator.getTaskEndpoint().showTaskByIndex(session, index);
        System.out.println("ID: " + task.getId());
        System.out.println("TITLE: " + task.getTitle());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("[OK]");
        System.out.println();
    }

}
