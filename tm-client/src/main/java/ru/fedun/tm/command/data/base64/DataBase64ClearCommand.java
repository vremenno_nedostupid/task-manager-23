package ru.fedun.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;

public final class DataBase64ClearCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-clear-base64";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear data from base64 file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 CLEAR]");
        @NotNull Session session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getDataEndpoint().clearBase64File(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
