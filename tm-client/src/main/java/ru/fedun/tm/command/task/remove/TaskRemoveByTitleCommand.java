package ru.fedun.tm.command.task.remove;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.command.AbstractCommand;
import ru.fedun.tm.endpoint.Session;
import ru.fedun.tm.util.TerminalUtil;

public final class TaskRemoveByTitleCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-title";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by title.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE TASK]");
        @NotNull final Session session = serviceLocator.getSessionService().getCurrentSession();
        System.out.println("[ENTER TITLE:]");
        @NotNull final String title = TerminalUtil.nextLine();
        serviceLocator.getTaskEndpoint().removeTaskByName(session, title);
        System.out.println("[OK]");
        System.out.println();
    }

}
