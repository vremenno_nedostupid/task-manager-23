package ru.fedun.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandService {

    @NotNull
    List<AbstractCommand> getCommands();

}
