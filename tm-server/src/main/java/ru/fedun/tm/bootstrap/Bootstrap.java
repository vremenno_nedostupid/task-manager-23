package ru.fedun.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.endpoint.*;
import ru.fedun.tm.api.repository.IProjectRepository;
import ru.fedun.tm.api.repository.ISessionRepository;
import ru.fedun.tm.api.repository.ITaskRepository;
import ru.fedun.tm.api.repository.IUserRepository;
import ru.fedun.tm.api.service.*;
import ru.fedun.tm.endpoint.*;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.repository.ProjectRepository;
import ru.fedun.tm.repository.SessionRepository;
import ru.fedun.tm.repository.TaskRepository;
import ru.fedun.tm.repository.UserRepository;
import ru.fedun.tm.service.*;

import javax.xml.ws.Endpoint;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(taskService, projectService, userService);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, this);

    @NotNull
    private final ILoadService loadService = new LoadService(this);

    @NotNull
    private final IAdminEndpoint adminEndpoint = new AdminEndpoint(this);

    @NotNull
    private final IDataEndpoint dataEndpoint = new DataEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    private void registry(final @NotNull Object endpoint) {
        final @NotNull String host = propertyService.getServerHost();
        final @NotNull Integer port = propertyService.getServerPort();
        final @NotNull String name = endpoint.getClass().getSimpleName();
        final @NotNull String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    private void initUsers() {
        userService.create("test", "test", "Ivan", "Ivanov", "test@mail.ru");
        userService.create("admin", "admin", "Petr", "Petrov", Role.ADMIN);
    }

    private void initEndpoint() {
        registry(adminEndpoint);
        registry(dataEndpoint);
        registry(projectEndpoint);
        registry(sessionEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
    }

    private void initProps() throws Exception{
        propertyService.init();
    }

    public void run() throws Exception{
        initProps();
        initUsers();
        initEndpoint();
        System.out.println("WELCOME TO TASK MANAGER");
    }

    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    public IDomainService getDomainService() {
        return domainService;
    }

    @NotNull
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @NotNull
    public ISessionService getSessionService() {
        return sessionService;
    }

    @NotNull
    public ILoadService getLoadService() {
        return loadService;
    }

}
