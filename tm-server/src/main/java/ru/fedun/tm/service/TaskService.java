package ru.fedun.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.repository.ITaskRepository;
import ru.fedun.tm.api.service.ITaskService;
import ru.fedun.tm.exception.empty.EmptyDescriptionException;
import ru.fedun.tm.exception.empty.EmptyIdException;
import ru.fedun.tm.exception.empty.EmptyTitleException;
import ru.fedun.tm.exception.empty.EmptyUserIdException;
import ru.fedun.tm.exception.incorrect.IncorrectIndexException;
import ru.fedun.tm.entity.Task;

import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    public TaskService(@NotNull ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @NotNull
    public List<Task> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        return taskRepository.findAll(userId);
    }

    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.clear(userId);
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final String title) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (title.isEmpty()) throw new EmptyTitleException();
        @NotNull final Task task = new Task();
        task.setTitle(title);
        taskRepository.add(userId, task);
    }

    @Override
    public void create(
            @NotNull final String userId,
            @NotNull final String title,
            @NotNull final String description) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (title.isEmpty()) throw new EmptyTitleException();
        if (description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setTitle(title);
        task.setDescription(description);
        taskRepository.add(userId, task);
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final Task task) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.add(userId, task);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Task task) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        taskRepository.remove(userId, task);
    }

    @NotNull
    @Override
    public Task getOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        return taskRepository.findOneById(userId, id);
    }

    @NotNull
    @Override
    public Task getOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IncorrectIndexException();
        return taskRepository.findOneByIndex(userId, index);
    }

    @NotNull
    @Override
    public Task getOneByTitle(@NotNull final String userId, @NotNull final String title) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (title.isEmpty()) throw new EmptyTitleException();
        return taskRepository.findOneByTitle(userId, title);
    }

    @NotNull
    @Override
    public Task updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String title,
            @NotNull final String description) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        if (title.isEmpty()) throw new EmptyTitleException();
        if (description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = getOneById(userId, id);
        task.setId(id);
        task.setTitle(title);
        task.setDescription(description);
        return task;
    }

    @NotNull
    @Override
    public Task updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String title,
            @NotNull final String description) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IncorrectIndexException();
        if (title.isEmpty()) throw new EmptyTitleException();
        if (description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = getOneByIndex(userId, index);
        task.setTitle(title);
        task.setDescription(description);
        return task;
    }

    @NotNull
    @Override
    public Task removeOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeOneById(userId, id);
    }

    @NotNull
    @Override
    public Task removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IncorrectIndexException();
        return taskRepository.removeOneByIndex(userId, index);
    }

    @NotNull
    @Override
    public Task removeOneByTitle(@NotNull final String userId, @NotNull final String title) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (title.isEmpty()) throw new EmptyTitleException();
        return taskRepository.removeOneByTitle(userId, title);
    }

}
