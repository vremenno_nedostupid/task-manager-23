package ru.fedun.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.api.repository.IUserRepository;
import ru.fedun.tm.api.service.IUserService;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.exception.empty.EmptyEmailException;
import ru.fedun.tm.exception.empty.EmptyIdException;
import ru.fedun.tm.exception.empty.EmptyLoginException;
import ru.fedun.tm.exception.empty.EmptyPassException;
import ru.fedun.tm.exception.incorrect.IncorrectPasswordException;
import ru.fedun.tm.exception.user.AccessDeniedException;
import ru.fedun.tm.util.HashUtil;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String firstName,
            @NotNull final String secondName) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPassException();
        if (firstName.isEmpty()) throw new EmptyLoginException();
        if (secondName.isEmpty()) throw new EmptyPassException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setFirstName(firstName);
        user.setSecondName(secondName);
        user.setRole(Role.USER);
        return userRepository.add(user);
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String firstName,
            @NotNull final String secondName,
            @NotNull final String email
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPassException();
        if (email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = create(login, password, firstName, secondName);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final String firstName,
            @NotNull final String secondName,
            @NotNull final Role role
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        if (password.isEmpty()) throw new EmptyPassException();
        @NotNull final User user = create(login, password, firstName, secondName);
        user.setRole(role);
        return user;
    }

    @NotNull
    @Override
    public User findById(@NotNull final String id) {
        if (id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @NotNull
    @Override
    public User findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @NotNull
    @Override
    public User updateMail(@NotNull final String userId, @Nullable final String email) {
        if (userId.isEmpty()) throw new AccessDeniedException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = findById(userId);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User updatePassword(
            @NotNull final String userId,
            @NotNull final String password,
            @NotNull final String newPassword
    ) {
        if (userId.isEmpty()) throw new AccessDeniedException();
        if (password.isEmpty()) throw new EmptyPassException();
        if (newPassword.isEmpty()) throw new EmptyPassException();
        @NotNull final User user = findById(userId);
        if (!HashUtil.salt(password).equals(user.getPasswordHash())) throw new IncorrectPasswordException();
        user.setPasswordHash(HashUtil.salt(newPassword));
        return user;
    }

    @Override
    public void removeById(@NotNull final String id) {
        if (id.isEmpty()) throw new EmptyIdException();
        userRepository.removeById(id);
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        userRepository.removeByLogin(login);
    }

    @Override
    public void removeUser(@NotNull final String userId, @NotNull final User user) {
        if (userId.isEmpty()) throw new AccessDeniedException();
        userRepository.remove(user);
    }

    @NotNull
    @Override
    public User lockUserByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NotNull final User user = findByLogin(login);
        user.setLocked(true);
        return user;
    }

    @NotNull
    @Override
    public User unlockUserByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NotNull final User user = findByLogin(login);
        user.setLocked(false);
        return user;
    }

}
