package ru.fedun.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.api.repository.ISessionRepository;
import ru.fedun.tm.api.service.IPropertyService;
import ru.fedun.tm.api.service.ISessionService;
import ru.fedun.tm.api.service.ServiceLocator;
import ru.fedun.tm.entity.Session;
import ru.fedun.tm.entity.User;
import ru.fedun.tm.enumerated.Role;
import ru.fedun.tm.exception.user.AccessDeniedException;
import ru.fedun.tm.util.HashUtil;
import ru.fedun.tm.util.SignatureUtil;

import java.util.List;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    private final ServiceLocator serviceLocator;

    private final ISessionRepository sessionRepository;

    public SessionService(
            final ISessionRepository sessionRepository,
            final ServiceLocator serviceLocator) {
        super(sessionRepository);
        this.serviceLocator = serviceLocator;
        this.sessionRepository = sessionRepository;
    }

    @Override
    public boolean checkDataAccess(@NotNull final String login, @NotNull final String password) {
        if (login.isEmpty()) return false;
        if (password.isEmpty()) return false;
        @NotNull final User user = serviceLocator.getUserService().findByLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    public boolean isValid(@NotNull final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(@NotNull final Session session) throws Exception {
        if (session.getSignature() == null) throw new AccessDeniedException();
        if (session.getStartTime() == null) throw new AccessDeniedException();
        if (session.getUserId() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean checkSignature = signatureSource.equals(signatureTarget);
        if (!checkSignature) throw new AccessDeniedException();
        if (!sessionRepository.contains(session.getId())) throw new AccessDeniedException();
    }

    @Override
    public void validate(@NotNull final Session session, @NotNull final Role role) throws Exception {
        validate(session);
        @NotNull final String userId = session.getUserId();
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @NotNull
    @Override
    public Session open(@NotNull final String login, @NotNull final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        @NotNull final User user = serviceLocator.getUserService().findByLogin(login);
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setStartTime(System.currentTimeMillis());
        sessionRepository.add(session);
        return sign(session);
    }

    @Nullable
    @Override
    public Session sign(@NotNull final Session session) {
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        return session;
    }

    @NotNull
    @Override
    public User getUser(@NotNull final Session session) throws Exception {
        @NotNull final String userId = getUserId(session);
        return serviceLocator.getUserService().findById(userId);
    }

    @NotNull
    @Override
    public String getUserId(@NotNull final Session session) throws Exception {
        validate(session);
        return session.getUserId();
    }

    @NotNull
    @Override
    public List<Session> getSessionList(@NotNull final Session session) throws Exception {
        validate(session);
        return sessionRepository.findAllSessions(session.getUserId());
    }

    @Override
    public void close(@NotNull final Session session) throws Exception {
        validate(session);
        sessionRepository.removeByUserId(session.getUserId());
    }

    @Override
    public void closeAll(@NotNull final Session session) throws Exception {
        validate(session);
        sessionRepository.clear();
    }

    @Override
    public void signOutByLogin(@NotNull final String login) throws Exception {
        if (login.isEmpty()) throw new AccessDeniedException();
        @NotNull final User user = serviceLocator.getUserService().findByLogin(login);
        @NotNull final String userId = user.getId();
        sessionRepository.removeByUserId(userId);
    }

    @Override
    public void signOutByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new AccessDeniedException();
        sessionRepository.removeByUserId(userId);
    }

}
