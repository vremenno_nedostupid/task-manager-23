package ru.fedun.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.repository.IProjectRepository;
import ru.fedun.tm.api.service.IProjectService;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.exception.empty.EmptyDescriptionException;
import ru.fedun.tm.exception.empty.EmptyIdException;
import ru.fedun.tm.exception.empty.EmptyTitleException;
import ru.fedun.tm.exception.empty.EmptyUserIdException;
import ru.fedun.tm.exception.incorrect.IncorrectIndexException;

import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectService(@NotNull IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final Project project) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.add(userId, project);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final Project project) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.remove(userId, project);
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final String title, @NotNull final String description) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (title.isEmpty()) throw new EmptyTitleException();
        if (description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        project.setTitle(title);
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final String title) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (title.isEmpty()) throw new EmptyTitleException();
        @NotNull final Project project = new Project();
        project.setTitle(title);
        projectRepository.add(userId, project);
    }

    @NotNull
    public List<Project> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepository.findAll(userId);
    }

    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.clear(userId);
    }

    @NotNull
    @Override
    public Project getOneById(@NotNull final String userId, @NotNull final String id) {
        if (id.isEmpty()) throw new EmptyIdException();
        if (userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepository.findOneById(userId, id);
    }

    @NotNull
    @Override
    public Project getOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IncorrectIndexException();
        return projectRepository.findOneByIndex(userId, index);
    }

    @NotNull
    @Override
    public Project getOneByTitle(@NotNull final String userId, @NotNull final String title) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (title.isEmpty()) throw new EmptyTitleException();
        return projectRepository.findOneByTitle(userId, title);
    }

    @NotNull
    @Override
    public Project updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String title,
            @NotNull final String description
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        if (title.isEmpty()) throw new EmptyTitleException();
        if (description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Project project = getOneById(userId, id);
        project.setId(id);
        project.setTitle(title);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String title,
            @NotNull final String description
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IncorrectIndexException();
        if (title.isEmpty()) throw new EmptyTitleException();
        if (description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Project project = getOneByIndex(userId, index);
        project.setTitle(title);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project removeOneById(@NotNull final String userId, @NotNull final String id) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeOneById(userId, id);
    }

    @NotNull
    @Override
    public Project removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IncorrectIndexException();
        return projectRepository.removeOneByIndex(userId, index);
    }

    @NotNull
    @Override
    public Project removeOneByTitle(@NotNull final String userId, @NotNull final String title) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (title.isEmpty()) throw new EmptyTitleException();
        return projectRepository.removeOneByTitle(userId, title);
    }

}
