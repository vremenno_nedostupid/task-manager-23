package ru.fedun.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.repository.IRepository;
import ru.fedun.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> entities = new ArrayList<>();

    @NotNull
    public List<E> findAll() {
        return entities;
    }

    @NotNull
    public E add(@NotNull E e) {
        entities.add(e);
        return e;
    }

    public void add(@NotNull final List<E> es) {
        entities.addAll(es);
    }

    public void add(@NotNull final E... es) {
        entities.addAll(Arrays.asList(es));
    }

    public void clear() {
        entities.clear();
    }

    public void load(@NotNull final E... es) {
        clear();
        add(es);
    }

    public void load(@NotNull final List<E> es) {
        clear();
        add(es);
    }

    public List<E> getEntities() {
        return new ArrayList<>(entities);
    }

}
