package ru.fedun.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.entity.Project;

import java.util.List;

public interface IProjectService extends IService<Project>{

    void create(@NotNull String userId, @NotNull String title);

    void create(@NotNull String userId, @NotNull String title, @NotNull String description);

    void add(@NotNull String userId, @NotNull Project project);

    void remove(@NotNull String userId, @NotNull Project project);

    @NotNull
    List<Project> findAll(@NotNull String userId);

    void clear(@NotNull String userId);

    @NotNull
    Project getOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project getOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project getOneByTitle(@NotNull String userId, @NotNull String title);

    @NotNull
    Project updateById(
            @NotNull String userId,
            @NotNull String id,
            @NotNull String title,
            @NotNull String description
    );

    @NotNull
    Project updateByIndex(
            @NotNull String userId,
            @NotNull Integer index,
            @NotNull String title,
            @NotNull String description
    );

    @NotNull
    Project removeOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    Project removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    Project removeOneByTitle(@NotNull String userId, @NotNull String title);

}
