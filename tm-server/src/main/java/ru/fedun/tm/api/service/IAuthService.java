package ru.fedun.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fedun.tm.enumerated.Role;

public interface IAuthService {

    @NotNull
    String getUserId();

    void login(@NotNull String login, @NotNull String password);

    void registry(
            @NotNull String login,
            @NotNull String password,
            @NotNull String firstName,
            @NotNull String secondName,
            @NotNull String email
    );

    void logout();

    boolean isAuth();

    void checkRole(@Nullable Role[] roles);

}
