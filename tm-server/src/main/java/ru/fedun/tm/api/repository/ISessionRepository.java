package ru.fedun.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session>{

    void add(@NotNull String userId, @NotNull Session session);

    void remove(@NotNull String userId, @NotNull Session session);

    void clear(@NotNull String userId);

    @NotNull
    List<Session> findAllSessions(@NotNull String userId);

    @NotNull
    Session findByUserId(@NotNull String userId) throws Exception;

    @NotNull
    Session findById(@NotNull String id) throws Exception;

    @NotNull
    Session removeByUserId(@NotNull String userId) throws Exception;

    boolean contains(@NotNull String id) throws Exception;

}
