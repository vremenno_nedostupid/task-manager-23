package ru.fedun.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.entity.Session;

public interface IDataEndpoint {

    void saveXmlByJaxb(@NotNull Session session) throws Exception;

    void loadXmlByJaxb(@NotNull Session session) throws Exception;

    void clearXmlFileJaxb(@NotNull Session session) throws Exception;

    void saveXmlByFasterXml(@NotNull Session session) throws Exception;

    void loadXmlByFasterXml(@NotNull Session session) throws Exception;

    void clearXmlFileFasterXml(@NotNull Session session) throws Exception;

    void saveJsonByJaxb(@NotNull Session session) throws Exception;

    void loadJsonByJaxb(@NotNull Session session) throws Exception;

    void clearJsonFileJaxb(@NotNull Session session) throws Exception;

    void saveJsonByFasterXml(@NotNull Session session) throws Exception;

    void loadJsonByFasterXml(@NotNull Session session) throws Exception;

    void clearJsonFileFasterXml(@NotNull Session session) throws Exception;

    void saveBinary(@NotNull Session session) throws Exception;

    void loadBinary(@NotNull Session session) throws Exception;

    void clearBinaryFile(@NotNull Session session) throws Exception;

    void saveBase64(@NotNull Session session) throws Exception;

    void loadBase64(@NotNull Session session) throws Exception;

    void clearBase64File(@NotNull Session session) throws Exception;

}
