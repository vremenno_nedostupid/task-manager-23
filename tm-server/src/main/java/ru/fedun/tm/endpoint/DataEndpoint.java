package ru.fedun.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.fedun.tm.api.endpoint.IDataEndpoint;
import ru.fedun.tm.api.service.ServiceLocator;
import ru.fedun.tm.entity.Session;
import ru.fedun.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class DataEndpoint implements IDataEndpoint {

    private ServiceLocator serviceLocator;

    public DataEndpoint() {
    }

    public DataEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void saveXmlByJaxb(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getLoadService().saveXmlByJaxb();
    }

    @Override
    @WebMethod
    public void loadXmlByJaxb(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getLoadService().loadXmlByJaxb();
    }

    @Override
    @WebMethod
    public void clearXmlFileJaxb(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getLoadService().clearXmlFileJaxb();
    }

    @Override
    @WebMethod
    public void saveXmlByFasterXml(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getLoadService().saveXmlByFasterXml();
    }

    @Override
    @WebMethod
    public void loadXmlByFasterXml(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getLoadService().loadXmlByFasterXml();
    }

    @Override
    @WebMethod
    public void clearXmlFileFasterXml(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getLoadService().clearXmlFileFasterXml();
    }

    @Override
    @WebMethod
    public void saveJsonByJaxb(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getLoadService().saveJsonByJaxb();
    }

    @Override
    @WebMethod
    public void loadJsonByJaxb(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getLoadService().loadJsonByJaxb();
    }

    @Override
    @WebMethod
    public void clearJsonFileJaxb(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getLoadService().clearJsonFileJaxb();
    }

    @Override
    @WebMethod
    public void saveJsonByFasterXml(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getLoadService().saveJsonByFasterXml();
    }

    @Override
    @WebMethod
    public void loadJsonByFasterXml(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getLoadService().loadJsonByFasterXml();
    }

    @Override
    @WebMethod
    public void clearJsonFileFasterXml(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getLoadService().clearJsonFileFasterXml();
    }

    @Override
    @WebMethod
    public void saveBinary(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getLoadService().saveBinary();
    }

    @Override
    @WebMethod
    public void loadBinary(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getLoadService().loadBinary();
    }

    @Override
    @WebMethod
    public void clearBinaryFile(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getLoadService().clearBinaryFile();
    }

    @Override
    @WebMethod
    public void saveBase64(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getLoadService().saveBase64();
    }

    @Override
    @WebMethod
    public void loadBase64(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getLoadService().loadBase64();
    }

    @Override
    @WebMethod
    public void clearBase64File(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getLoadService().clearBase64File();
    }

}
