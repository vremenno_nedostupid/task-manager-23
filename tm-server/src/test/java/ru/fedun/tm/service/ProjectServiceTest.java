package ru.fedun.tm.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.fedun.tm.api.service.IProjectService;
import ru.fedun.tm.entity.Project;
import ru.fedun.tm.exception.empty.EmptyDescriptionException;
import ru.fedun.tm.exception.empty.EmptyIdException;
import ru.fedun.tm.exception.empty.EmptyTitleException;
import ru.fedun.tm.exception.empty.EmptyUserIdException;
import ru.fedun.tm.exception.incorrect.IncorrectIndexException;
import ru.fedun.tm.marker.UnitServerCategory;
import ru.fedun.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(UnitServerCategory.class)
public class ProjectServiceTest {

    private IProjectService projectService;

    @Before
    public void initService() {
        projectService = new ProjectService(new ProjectRepository());
    }

    @Test
    public void createTest() {
        projectService.create("001", "project");
        assertFalse(projectService.getEntity().isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void createTestEmptyUserIdException() {
        projectService.create("", "project");
    }

    @Test(expected = EmptyTitleException.class)
    public void createTestEmptyNameException() {
        projectService.create("001", "");
    }

    @Test
    public void createWithDescriptionTest() {
        projectService.create("001", "project", "description");
        assertFalse(projectService.getEntity().isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void createWithDescriptionTestEmptyUserIdException() {
        projectService.create("", "project", "description");
    }

    @Test(expected = EmptyTitleException.class)
    public void createWithDescriptionTestEmptyNameException() {
        projectService.create("001", "", "description");
    }

    @Test(expected = EmptyDescriptionException.class)
    public void createWithDescriptionTestEmptyDescriptionException() {
        projectService.create("001", "project", "");
    }

    @Test
    public void addTest() {
        final Project project = new Project();
        projectService.add("001", project);
        assertFalse(projectService.getEntity().isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void addTestEmptyUserIdException() {
        final Project project = new Project();
        projectService.add("", project);
    }

    @Test
    public void removeTest() {
        final Project project = new Project();
        projectService.add("001", project);
        assertFalse(projectService.getEntity().isEmpty());
        projectService.remove("001", project);
        assertTrue(projectService.getEntity().isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void removeTestEmptyUserIdException() {
        final Project project = new Project();
        projectService.add("001", project);
        assertFalse(projectService.getEntity().isEmpty());
        projectService.remove("", project);
    }

    @Test
    public void getAllTest() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        assertNotNull(projectService.findAll("001"));
    }

    @Test(expected = EmptyUserIdException.class)
    public void getAllTestEmptyUserIdException() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        List<Project> projects = projectService.findAll("");
    }

    @Test
    public void clearTest() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        projectService.clear("001");
        assertTrue(projectService.getEntity().isEmpty());
    }

    @Test(expected = EmptyUserIdException.class)
    public void clearTestEmptyUserIdException() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        projectService.clear("");
    }

    @Test
    public void getOneByIdTest() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        final String projectId = project1.getId();
        assertEquals(project1, projectService.getOneById("001", projectId));
    }

    @Test(expected = EmptyUserIdException.class)
    public void getOneByIdTestEmptyUserIdException() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        final String projectId = project1.getId();
        projectService.getOneById("", projectId);
    }

    @Test(expected = EmptyIdException.class)
    public void getOneByIdTestEmptyIdException() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        projectService.getOneById("001", "");
    }

    @Test
    public void getOneByIndexTest() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        assertEquals(project1, projectService.getOneByIndex("001", 0));
    }

    @Test(expected = EmptyUserIdException.class)
    public void getOneByIndexTestEmptyUserIdException() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        projectService.getOneByIndex("", 0);
    }

    @Test(expected = IncorrectIndexException.class)
    public void getOneByIndexTestIncorrectIndexException() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        projectService.getOneByIndex("001", -2);
    }

    @Test
    public void getOneByNameTest() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        project1.setTitle("project1");
        assertEquals(project1, projectService.getOneByTitle("001", "project1"));
    }

    @Test(expected = EmptyUserIdException.class)
    public void getOneByNameTestEmptyUserIdException() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        project1.setTitle("project1");
        projectService.getOneByTitle("", "project1");
    }

    @Test(expected = EmptyTitleException.class)
    public void getOneByNameTestEmptyNameException() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        project1.setTitle("project1");
        projectService.getOneByTitle("001", "");
    }

    @Test
    public void removeOneByIdTest() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        final String projectId = project1.getId();
        projectService.removeOneById("001", projectId);
        assertFalse(projectService.getEntity().contains(project1));
    }

    @Test(expected = EmptyUserIdException.class)
    public void removeOneByIdTestEmptyUserIdException() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        final String projectId = project1.getId();
        projectService.removeOneById("", projectId);
    }

    @Test(expected = EmptyIdException.class)
    public void removeOneByIdTestEmptyIdException() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        projectService.removeOneById("001", "");
    }

    @Test
    public void removeOneByIndexTest() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        projectService.removeOneByIndex("001", 0);
        assertFalse(projectService.getEntity().contains(project1));
    }

    @Test(expected = EmptyUserIdException.class)
    public void removeOneByIndexTestEmptyUserIdException() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        projectService.removeOneByIndex("", 0);
    }

    @Test(expected = IncorrectIndexException.class)
    public void removeOneByIndexTestIncorrectIndexException() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        projectService.removeOneByIndex("001", -1);
    }

    @Test
    public void removeOneByNameTest() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        project1.setTitle("project1");
        projectService.removeOneByTitle("001", "project1");
        assertFalse(projectService.getEntity().contains(project1));
    }

    @Test(expected = EmptyUserIdException.class)
    public void removeOneByNameTestEmptyUserIdException() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        project1.setTitle("project1");
        projectService.removeOneByTitle("", "project1");
    }

    @Test(expected = EmptyTitleException.class)
    public void removeOneByNameTestEmptyNameException() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        project1.setTitle("project1");
        projectService.removeOneByTitle("001", "");
    }

    @Test
    public void updateProjectByIdTest() {
        final Project project = new Project();
        projectService.add("001", project);
        final String projectId = project.getId();
        project.setTitle("projectName");
        project.setDescription("description");
        final String projectName = "newProjectName";
        final String projectDescription = "newDescription";
        projectService.updateById("001", projectId, projectName, projectDescription);
        assertEquals(project.getTitle(), projectName);
        assertEquals(project.getDescription(), projectDescription);
    }

    @Test(expected = EmptyUserIdException.class)
    public void updateProjectByIdTestEmptyUserIdException() {
        final Project project = new Project();
        projectService.add("001", project);
        final String projectId = project.getId();
        project.setTitle("projectName");
        project.setDescription("description");
        final String projectName = "newProjectName";
        final String projectDescription = "newDescription";
        projectService.updateById("", projectId, projectName, projectDescription);
    }

    @Test(expected = EmptyIdException.class)
    public void updateProjectByIdTestEmptyIdException() {
        final Project project = new Project();
        projectService.add("001", project);
        project.setTitle("projectName");
        project.setDescription("description");
        final String projectName = "newProjectName";
        final String projectDescription = "newDescription";
        projectService.updateById("001", "", projectName, projectDescription);
    }

    @Test(expected = EmptyTitleException.class)
    public void updateProjectByIdTestEmptyNameException() {
        final Project project = new Project();
        projectService.add("001", project);
        final String projectId = project.getId();
        project.setTitle("projectName");
        project.setDescription("description");
        final String projectDescription = "newDescription";
        projectService.updateById("001", projectId, "", projectDescription);
    }

    @Test(expected = EmptyDescriptionException.class)
    public void updateProjectByIdTestEmptyDescriptionException() {
        final Project project = new Project();
        projectService.add("001", project);
        final String projectId = project.getId();
        project.setTitle("projectName");
        project.setDescription("description");
        final String projectName = "newProjectName";
        projectService.updateById("001", projectId, projectName, "");
    }

    @Test
    public void updateProjectByIndexTest() {
        final Project project = new Project();
        projectService.add("001", project);
        project.setTitle("projectName");
        project.setDescription("description");
        final String projectName = "newProjectName";
        final String projectDescription = "newDescription";
        projectService.updateByIndex("001", 0, projectName, projectDescription);
        assertEquals(project.getTitle(), projectName);
        assertEquals(project.getDescription(), projectDescription);
    }

    @Test(expected = EmptyUserIdException.class)
    public void updateProjectByIndexTestEmptyUserIdException() {
        final Project project = new Project();
        projectService.add("001", project);
        project.setTitle("projectName");
        project.setDescription("description");
        final String projectName = "newProjectName";
        final String projectDescription = "newDescription";
        projectService.updateByIndex("", 0, projectName, projectDescription);
    }

    @Test(expected = IncorrectIndexException.class)
    public void updateProjectByIndexTestIncorrectIndexException() {
        final Project project = new Project();
        projectService.add("001", project);
        project.setTitle("projectName");
        project.setDescription("description");
        final String projectName = "newProjectName";
        final String projectDescription = "newDescription";
        projectService.updateByIndex("001", -2, projectName, projectDescription);
    }

    @Test(expected = EmptyTitleException.class)
    public void updateProjectByIndexTestEmptyNameException() {
        final Project project = new Project();
        projectService.add("001", project);
        project.setTitle("projectName");
        project.setDescription("description");
        final String projectDescription = "newDescription";
        projectService.updateByIndex("001", 0, "", projectDescription);
    }

    @Test(expected = EmptyDescriptionException.class)
    public void updateProjectByIndexTestEmptyDescriptionException() {
        final Project project = new Project();
        projectService.add("001", project);
        project.setTitle("projectName");
        project.setDescription("description");
        final String projectName = "newProjectName";
        projectService.updateByIndex("001", 0, projectName, "");
    }

    @Test
    public void getListTest() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.add("001", project1);
        projectService.add("001", project2);
        assertNotNull(projectService.getEntity());
    }

    @Test
    public void loadVarargsTest() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.load(project1, project2);
        assertNotNull(projectService.getEntity());
    }

    @Test
    public void loadCollectionTest() {
        final List<Project> projects = new ArrayList<>();
        final Project project1 = new Project();
        projects.add(project1);
        final Project project2 = new Project();
        projects.add(project2);
        projectService.load(projects);
        assertNotNull(projectService.getEntity());
    }

    @Test
    public void clearWithoutUserIdTest() {
        final Project project1 = new Project();
        final Project project2 = new Project();
        projectService.load(project1, project2);
        projectService.clear();
        assertTrue(projectService.getEntity().isEmpty());
    }

}
